// The latitude and longitude of your business / place
const position = [27.1959739, 78.02423269999997];

export default function showGoogleMaps() {

    const latLng = new google.maps.LatLng(position[0], position[1]);

    const mapOptions = {
        zoom: 16, // initialize zoom level - the max value is 21
        streetViewControl: false, // hide the yellow Street View pegman
        scaleControl: true, // allow users to zoom the Google Map
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: latLng,
        styles: [
          {
            "stylers": [
              {
                "hue":  "#637273"
              },
              {
                "saturation": -40
              },
              {
                "lightness": -30
              }
            ]
          }
        ]
    };

    const map = new google.maps.Map(document.getElementById('googleMap'),
        mapOptions);

    // Show the default red marker at the location
    const marker = new google.maps.Marker({
        position: latLng,
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP
    });
}
