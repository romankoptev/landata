var gulp   = require('gulp');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

module.exports = function() {
  browserSync.init({
    server: {
      baseDir: [
        'build'
      ],
      serveStaticOptions: {
        extensions: ['html']
      }
    },
    open: false
  });

  gulp.watch('build/**/*.html').on('change', reload);
};

module.exports.dependencies = [
  'panini',
  'sass',
  'browserify',
  'minify:images',
  'fonts'
];
