var gulp   = require('gulp');
var panini = require('panini');
var browserSync = require('browser-sync')

module.exports = function() {
  gulp.watch('app/styles/**/*.scss', ['sass']);
  gulp.watch('app/**/*.html', ['refresh', 'panini']);
  // gulp.watch('build/**/*.html', ['browserRefresh']);
  gulp.watch('app/scripts/**/*.js', ['browserify']);
  gulp.watch('app/images/**/*', ['minify:images']);
};

gulp.task('refresh', function() {
  return panini.refresh();
});

gulp.task('browserRefresh', function() {
  return browserSync.refresh();
});

module.exports.dependencies = ['browser-sync'];
