var gulp       = require('gulp');
var sass       = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

module.exports = function() {
  return gulp.src('app/styles/app.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths: [
        'node_modules/bootstrap/scss',
        'node_modules/font-awesome/scss',
        'app/styles'
      ]
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/styles'));
};
